---
title: Nous contacter
---

# Nous contacter

**Editions REPAS**

4, allée Séverine
26 000 VALENCE
Tel : 04 75 42 67 45
E-mail : [nous contacter](mailto:repas@wanadoo.fr)

Pour connaître nos tarifs d'expédition et conditions de vente à l'usage de libraires et professionnels, merci de lire notre page [Conditions de vente](/conditions-de-vente/) ou [nous contacter](mailto:repas@wanadoo.fr).

Pour prendre connaissance de nos Conditions Générales de Vente, merci de vous rendre sur la [page Mentions Légales](/mentions-legales/).

## Inscription à la lettre des Editions REPAS

Vous souhaitez être informé des dernières nouveautés des Editions REPAS ?
Ecrivez-nous par mail via [nous contacter](mailto:repas@wanadoo.fr).

Vous recevrez quatre fois par an notre lettre d'information avec toutes les actualités des Editions REPAS, les prochains livres, nos projets et nos participations sur les foires, salons et festivals dans toute la France.

![insert image editions-repas.png](/assets/editions-repas.png)
